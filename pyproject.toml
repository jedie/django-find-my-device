[project]
name = "django-fmd"
dynamic = ["version"]
description = "Server for 'Find My Device' android app, implemented in Django/Python"
license = {text = "GPL-3.0-or-later"}
readme = "README.md"
authors = [
    {name = 'Jens Diemer', email = 'git@jensdiemer.de'}
]
requires-python = ">=3.9"
dependencies = [
    "colorlog",  # https://github.com/borntyping/python-colorlog
    "gunicorn",  # https://github.com/benoimyproject.wsgitc/gunicorn

    "django",
    "django-debug-toolbar",  # http://django-debug-toolbar.readthedocs.io/en/stable/changes.html
    "bx_py_utils",  # https://github.com/boxine/bx_py_utils
    "bx_django_utils",  # https://github.com/boxine/bx_django_utils

    "requests",  # https://github.com/psf/requests
    "pycryptodomex",  # https://github.com/Legrandin/pycryptodome
    "argon2-cffi",  # https://github.com/hynek/argon2-cffi
]
[project.optional-dependencies]
dev = [
    "manage_django_project",  # https://github.com/jedie/manage_django_project
    "beautifulsoup4",  # https://www.crummy.com/software/BeautifulSoup/
    "tblib",   # https://github.com/ionelmc/python-tblib
    "pip-tools",  # https://github.com/jazzband/pip-tools/
    "tox",  # https://github.com/tox-dev/tox
    "coverage",  # https://github.com/nedbat/coveragepy
    "autopep8",  # https://github.com/hhatto/autopep8
    "pyupgrade",  # https://github.com/asottile/pyupgrade
    "flake8",  # https://github.com/pycqa/flake8
    "flake8-bugbear",  # https://github.com/PyCQA/flake8-bugbear
    "pyflakes",  # https://github.com/PyCQA/pyflakes
    "codespell",  # https://github.com/codespell-project/codespell
    "EditorConfig",  # https://github.com/editorconfig/editorconfig-core-py
    "safety",  # https://github.com/pyupio/safety
    "mypy",  # https://github.com/python/mypy
    "twine",  # https://github.com/pypa/twine

    # https://github.com/akaihola/darker
    # https://github.com/ikamensh/flynt
    # https://github.com/pycqa/isort
    # https://github.com/pygments/pygments
    "darker[flynt, isort, color]",

    "tomli",  # https://github.com/hukkin/tomli
    # tomli only needed for Python <3.11, but see bug:
    # https://github.com/pypa/pip/issues/9644#issuecomment-1456583402
    #"tomli;python_version<\"3.11\"",  # https://github.com/hukkin/tomli

    # Work-a-round for:
    # https://github.com/jazzband/pip-tools/issues/994#issuecomment-1321226661
    "typing-extensions>=3.10",

    "model_bakery",  # https://github.com/model-bakers/model_bakery
    "requests-mock",
    "django-override-storage",  # https://github.com/danifus/django-override-storage
]

[project.urls]
Documentation = "https://gitlab.com/jedie/django-find-my-device"
Source = "https://gitlab.com/jedie/django-find-my-device"


[project.scripts]
# Must be set in ./manage.py and PROJECT_SHELL_SCRIPT:
findmydevice_project = "findmydevice_project.__main__:main"

[manage_django_project]
module_name="findmydevice_project"

# Django settings used for all commands except test/coverage/tox:
local_settings='findmydevice_project.settings.local'

# Django settings used for test/coverage/tox commands:
test_settings='findmydevice_project.settings.tests'


[build-system]
requires = ["setuptools>=61.0", "setuptools_scm>=7.1"]
build-backend = "setuptools.build_meta"

[tool.setuptools.packages.find]
where = ["."]
include = ["findmydevice*", "findmydevice_project*"]

[tool.setuptools.dynamic]
version = {attr = "findmydevice.__version__"}


[tool.darker]
src = ['.']
revision = "origin/main..."
line_length = 119
color = true
skip_string_normalization = true
diff = false
check = false
stdout = false
isort = true
lint = [
    "flake8",
]
log_level = "INFO"


[tool.isort]
# https://pycqa.github.io/isort/docs/configuration/config_files/#pyprojecttoml-preferred-format
atomic=true
profile='black'
skip_glob=['.*', '*/htmlcov/*','*/migrations/*']
known_first_party=['findmydevice']
line_length=119
lines_after_imports=2


[tool.coverage.run]
branch = true
parallel = true
concurrency = ["multiprocessing"]
source = ['.']
command_line = '-m findmydevice_project test --shuffle --parallel --buffer'

[tool.coverage.report]
omit = ['.*', '*/tests/*', '*/migrations/*']
skip_empty = true
fail_under = 30
show_missing = true
exclude_lines = [
    'if self.debug:',
    'pragma: no cover',
    'raise NotImplementedError',
    'if __name__ == .__main__.:',
]


[tool.tox]  # https://tox.wiki/en/latest/config.html#pyproject-toml
legacy_tox_ini = """
[tox]
isolated_build = True
envlist = py{312,311,310}
skip_missing_interpreters = True

[testenv]
passenv = *
skip_install = true
commands_pre =
    pip install -U pip-tools
    pip-sync requirements.dev.txt
commands =
    {envpython} -m coverage run --context='{envname}' -m findmydevice_project test --buffer --shuffle --parallel
"""


[tool.mypy]
warn_unused_configs = true
ignore_missing_imports = true
allow_redefinition = true  # https://github.com/python/mypy/issues/7165
show_error_codes = true
plugins = []
exclude = ['.venv', 'tests', 'migrations']


[manageprojects] # https://github.com/jedie/manageprojects
initial_revision = "1f3a70e"
initial_date = 2024-05-21T21:22:39+02:00
cookiecutter_template = "https://github.com/jedie/cookiecutter_templates/"
cookiecutter_directory = "managed-django-project"

[manageprojects.cookiecutter_context.cookiecutter]
full_name = "Jens Diemer"
github_username = "jedie"
author_email = "git@jensdiemer.de"
project_name = "django-fmd"
package_name = "findmydevice"
django_project_name = "findmydevice_project"
package_version = "0.4.0"
package_description = "Server for 'Find My Device' android app, implemented in Django/Python"
package_url = "https://gitlab.com/jedie/django-find-my-device"
license = "GPL-3.0-or-later"
_template = "https://github.com/jedie/cookiecutter_templates/"
