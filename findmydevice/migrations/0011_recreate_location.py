# Generated by Django 5.0.6 on 2024-06-16 09:43

import uuid

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('findmydevice', '0010_expand_field_length'),
    ]

    operations = [
        migrations.DeleteModel(
            name='location',
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                (
                    'create_dt',
                    models.DateTimeField(
                        blank=True,
                        editable=False,
                        help_text='ModelTimetrackingMixin.create_dt.help_text',
                        null=True,
                        verbose_name='ModelTimetrackingMixin.create_dt.verbose_name',
                    ),
                ),
                (
                    'update_dt',
                    models.DateTimeField(
                        blank=True,
                        editable=False,
                        help_text='ModelTimetrackingMixin.update_dt.help_text',
                        null=True,
                        verbose_name='ModelTimetrackingMixin.update_dt.verbose_name',
                    ),
                ),
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('user_agent', models.CharField(blank=True, editable=False, max_length=512, null=True)),
                ('data', models.TextField(unique=True)),
                ('device', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='findmydevice.device')),
            ],
            options={
                'get_latest_by': ['create_dt'],
            },
        ),
    ]
